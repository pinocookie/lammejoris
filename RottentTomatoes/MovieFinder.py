import requests
from bs4 import BeautifulSoup
import re
genre_string = input('What genre? ')
genre_int = 0
def genreConversion():
    global genre_int
    if genre_string == 'action':
        genre_int = 1
    elif genre_string == 'animation':
        genre_int = 2
    elif genre_string == 'art':
        genre_int = 4
    elif genre_string == 'classics':
        genre_int = 5
    elif genre_string == 'comedy':
        genre_int = 6
    elif genre_string == 'documentary':
        genre_int = 8
    elif genre_string == 'drama':
        genre_int = 9
    elif genre_string == 'horror':
        genre_int = 10
    elif genre_string == 'kids':
        genre_int = 11
    elif genre_string == 'mystery':
        genre_int = 13
    elif genre_string == 'romance':
        genre_int = 18
    elif genre_string == 'scifi':
        genre_int = 14

genreConversion()
genre = str(genre_int)
url = 'https://www.rottentomatoes.com/browse/dvd-streaming-all?minTomato=0&maxTomato=100&services=amazon;hbo_go;itunes;netflix_iw;vudu;amazon_prime;fandango_now&genres=' + genre
print(url)
site = requests.get(url)
sexy_url = BeautifulSoup(site.text, 'html.parser')
#print(sexy_url.prettify())
script_header = sexy_url.find_all('script')
script = script_header[38]
print(script)
#print(script_header)